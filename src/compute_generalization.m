function [ posteriors ] = compute_generalization( new_data, data, priors, H )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

%compute the likelihoods
likelihoods = [likelihood(H(1), [data;new_data]);
               likelihood(H(2), [data;new_data]);     
               likelihood(H(3), [data;new_data]);
               likelihood(H(4), [data;new_data]);
               likelihood(H(5), [data;new_data]);
               likelihood(H(6), [data;new_data]);     
               likelihood(H(7), [data;new_data]);
               likelihood(H(8), [data;new_data]);
               likelihood(H(9), [data;new_data]);
               likelihood(H(10), [data;new_data])
               ];
           
%posterior probability is the prior times the likelihood
posteriors = priors .* likelihoods;
%posteriors_norm = posteriors / norm(posteriors,1);
end

