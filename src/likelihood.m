function [ ret ] = likelihood( hypothesis, data )
%likelihood Calculate the likelihood of the data being shown (data), given model (hypothesis) 
    % assuming square of size hypothesis, centered on origin
    minX = -hypothesis;
    minY = -hypothesis;
    maxX = hypothesis;
    maxY = hypothesis;
    num_samples = size(data, 1);

    % check if the sample is within the square produced by hypothesis
    illegal_sample_found = false;
    for n = 1:num_samples

        % check the X
        if data(n) < minX || data(n) > maxX
            illegal_sample_found = true;
            break; % hypothesis cannot be true - however, it could still be a good one? just not in this algo
        % check the Y
        elseif data(n, 2) < minY || data(n,2) > maxY
            illegal_sample_found = true;
            break;

        end;
    end;

    % if the sample is not within the square, the hypothesis cannot be
    % true, so the likelihood is 0
    if illegal_sample_found
        szStr = ['Sample found that disproves hypothesis ', num2str(hypothesis), ' - with data ' , num2str(data(n,:))];
        %disp(szStr);
        ret = 0; % again, maybe a good algo wouldn't discount this model entirely
    else
        ret = 1 / (((2*hypothesis)^2)^num_samples);
    end

end

