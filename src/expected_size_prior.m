% function [ ret ] = expected_size_prior( sz, data )
% %UNTITLED2 Summary of this function goes here
% %   Detailed explanation goes here
% 
% std_dev = std(data, 0, 1);
% std_dev_1 = std_dev(1);
% std_dev_2 = std_dev(2);
% ret = exp(-( (sz/std_dev_1) + (sz/std_dev_2) ))
% end

function [ ret ] = expected_size_prior( sz_1, sz_2, sigma_1, sigma_2)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

%std_dev = std(data, 0, 1);
ret = exp(-( (sz_1/sigma_1) + (sz_2/sigma_2) ));
end