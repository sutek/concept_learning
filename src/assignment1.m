format LONGE;
rng('default') % start off with a predictable set

%data

%random data
%num_samples = 1
%data = randi([-3,3]*10,[num_samples,2])/10; % not sure how to derive this!

%task 2 - (-1.5, 0.5)
data=zeros(1,2);
%task 3
%data(1,:) = [1.5, 0.5];

%task 4
%data(1,:) = [4.5,2.5];

%task 5
%data(1,:) = [2.2, -2.0];
%data(2,:) = [0.5, 0.5];
%data(3,:) = [1.5,1];
%data(4,:) = [1.0,1];
%data(5,:) = [1.5,1.1];
% data(6,:) = [1.5,1.7];
% data(7,:) = [1.5,1.9];
% data(8,:) = [1.5,2];
% data(9,:) = [1.6,1.9];
% data(10,:) = [1.7,1.9];
% data(11,:) = [7.7,1.9];
sigma=20;

%task 6
%data(1,:) = [2.2, -2.0];

% hypothesis, concept square i=1 (-1, 1)

h1 = 1;
h2 = 2;
h3 = 3;
h4 = 4;
h5 = 5;
h6 = 6;
h7 = 7;
h8 = 8;
h9 = 9;
h10 = 10;
H = [h1;h2;h3;h4;h5;h6;h7;h8;h9;10];


% uninformative prior (need to replace with Expected Size prior
priors_exp = [
    expected_size_prior(2*h1, 2*h1, sigma, sigma);
    expected_size_prior(2*h2, 2*h2, sigma, sigma);
    expected_size_prior(2*h3, 2*h3, sigma, sigma);
    expected_size_prior(2*h4, 2*h4, sigma, sigma);
    expected_size_prior(2*h5, 2*h5, sigma, sigma);
    expected_size_prior(2*h6, 2*h6, sigma, sigma);
    expected_size_prior(2*h7, 2*h7, sigma, sigma);
    expected_size_prior(2*h8, 2*h8, sigma, sigma);
    expected_size_prior(2*h9, 2*h9, sigma, sigma);
    expected_size_prior(2*h10, 2*h10, sigma, sigma)];

priors_uninf = [   
            uninformative_prior(2*h1);
            uninformative_prior(2*h2);
            uninformative_prior(2*h3);
            uninformative_prior(2*h4);
            uninformative_prior(2*h5);
            uninformative_prior(2*h6);
            uninformative_prior(2*h7);
            uninformative_prior(2*h8);
            uninformative_prior(2*h9);
            uninformative_prior(2*h10)];

priors_uninf_normalized=priors_uninf/norm(priors_uninf,1);
priors_exp_normalized=priors_exp/norm(priors_exp,1);
%test that the priors are correctly normalized
% if sum(priors_normalized) ~= 1
%     disp('Error - priors are not normalized correctly');
% end
figure();
bar(priors_uninf_normalized);
title('Uninformed priors');
xlabel('Hypothesis')
ylabel('Prior Probability P(H)')
figure();
bar(priors_exp_normalized);


%set(gca,'YScale','log') % not needed 
title (['Task 1 - Prior probabilities for sigma: ', num2str(sigma)])
xlabel('Hypothesis')
ylabel('Prior Probability P(H)')

figure();
%posteriors = compute_generalization(data, [], priors, H)

% task 2
% bar(posterior);
% title ('Task 2 - Posteriors for \{1.5, 0.5\}')
% xlabel('Hypothesis')
% ylabel('Posterior Probability - P(H|X)')

%task 3
%  x=linspace(-10,10);
%  y=linspace(-10,10);
%input_coords = [rng, rng];
%grid(-7,7) = 
increment_factor = 1;
allowed_range=-10:increment_factor:10;
contour_map_exp = zeros(size(allowed_range,2), size(allowed_range,2));
contour_map_uninf = zeros(size(allowed_range,2), size(allowed_range,2));

output_x = 0;
output_y = 0;

for x = -10:increment_factor:10
    output_x = output_x+1;
    output_y = 0;
    for y = -10:increment_factor:10
        output_y = output_y+1;
        %szStr = [num2str(row), ' ', num2str(column)]
        %disp(szStr);
        %find gets the non-zero hypotheses
        %min gets the most likely
        %put this most likely candidate in the Z axis of the contour map
        %contour_map(row, column) = min(find(compute_generalization(data, [-11+row, -11+column], sigma)));
        %I've commented out the above, as it finds the most likely
        %categorization - the problem asks for the probability
        %contour_map(row, column) = -log(max(compute_generalization(data, [-11+row, -11+column], sigma)));
        new_data = [x, y];
        generalizations = compute_generalization(data, new_data, priors_exp, H);
        log_generalizations = abs(log(generalizations));
        
        
        
        %log_generalizations = abs(log(compute_generalization(data, new_data, priors_uninf, H)));
        %contour_map_exp(output_y, output_x) = (sum(log_generalizations(~isinf(log_generalizations))));
        %contour_map_uninf(output_y, output_x) = (sum(log_generalizations(~isinf(log_generalizations))));
        contour_map_exp(output_y, output_x) = sum(compute_generalization(data, new_data, priors_exp,H));
        contour_map_uninf(output_y, output_x) = sum(compute_generalization(data, new_data, priors_uninf,H));

    end
end

contourf(-10:increment_factor:10, -10:increment_factor:10,log(contour_map_exp),'ShowText', 'on'); %, 'LevelList', unique(contour_map_exp));
grid on
szData = '\{';
for n = 1:size(data,1)
    szData = ([szData, num2str(data(n,:)), ', ']  ); 
end
szData = [szData, '\}'];
title (['Task 4 - Generalization Prediction expected size prior - (log p(y|X) ), X: ', szData, ' sigma: ', num2str(sigma)])
xlabel('Prediction X');
ylabel('Prediction Y');
axis([-11 11 -11 11])
hold on;
scatter(data(:,1),data(:,2))
colorbar;

figure();
[c2,hc2] = contourf(-10:increment_factor:10, -10:increment_factor:10,log(contour_map_uninf),'ShowText', 'on');
grid on
szData = '\{';
for n = 1:size(data,1)
    szData = ([szData, num2str(data(n,:)), ', ']  ); 
end
szData = [szData, '\}'];
title (['Task 6 - Generalization Prediction uninformed prior - (log p(y|X) ), X: ', szData])
xlabel('Prediction X');
ylabel('Prediction Y');
axis([-11 11 -11 11])
hold on;
scatter(data(:,1),data(:,2))
colorbar;

%couldnt get integration working
%assume(h >= 1 & h <= 10)
%F = int(1 / s - sigma, s, -10, 10)



